(function () {
    var player1 = document.getElementById('pl1'),
        player2 = document.getElementById('pl2'),
        buttonGeneration = document.getElementById('generation'),
        deleteStr = document.getElementsByClassName('delete')[0],
        wrapper = document.getElementsByClassName('wrapper')[0],
        wrapperRows = document.getElementsByClassName('wrapper_rows')[0],
        wrapperCols = document.getElementsByClassName('wrapper_cols')[0],
        matrPl1 = [], matrPl2 = [], isDeleteStr = false, tableElement,
        nameStrategy = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'], isPlayer = true, tableG, tableRows,
        tableColumns, compTable, check = {row: false, col: false};
    var from = document.getElementById('from').value;
    var to = document.getElementById('to').value;


    function GenerationTable(player1, player2) {

        tableG = document.createElement('table');
        tableG.classList.add('computer-table');
        var matrPl1Rows = [], matrPl2Rows = [];
        resetDate();
        wrapper.appendChild(tableG);


        for (let i = 0; i < player1; i++) {
            tableRows = document.createElement('tr');
            matrPl1Rows = [];
            matrPl2Rows = [];


            for (let j = 0; j < player2; j++) {


                tableColumns = document.createElement('td');
                matrPl1Rows.push(getRandomNumber());
                matrPl2Rows.push(getRandomNumber());

                tableColumns.innerHTML = matrPl1Rows[j] + ";" + matrPl2Rows[j];

                tableColumns.setAttribute("contenteditable", "true");
                tableRows.appendChild(tableColumns);


            }

            matrPl1.push(matrPl1Rows);
            matrPl2.push(matrPl2Rows);
            tableG.appendChild(tableRows);
        }

        deleteStr.classList.add('show');
        tableElement = document.body.getElementsByTagName('table')[0];
        compTable = tableG;


    }

    function GenerationTableForMe(player1, player2) {


        var tableG = document.createElement('table'),
            tableRows, tableColumns;
        tableG.classList.add('myTable');


        wrapper.appendChild(tableG);


        for (let i = 0; i < player1; i++) {
            tableRows = document.createElement('tr');
            for (let j = 0; j < player2; j++) {
                tableColumns = document.createElement('td');
                tableColumns.innerHTML = matrPl1[i][j] + ";" + matrPl2[i][j];
                tableColumns.setAttribute("contenteditable", "true");
                tableRows.appendChild(tableColumns);


            }
            tableG.appendChild(tableRows);
        }
        for (let i = 0; i < player1; i++) {
            var button = document.createElement('button');
            button.innerHTML += 'DEL';
            wrapperRows.appendChild(button);

        }
        for (let i = 0; i < player2; i++) {
            var button = document.createElement('button');


            button.innerHTML += 'DEL';

            wrapperCols.appendChild(button);
        }


        tableElement = document.body.getElementsByTagName('table')[0];


    }

    function updateMatrix() {
        var tableG = document.createElement('table'),
            tableRows, tableColumns;
        document.body.appendChild(tableG);


        for (let i = 0; i < matrPl1.length; i++) {
            tableRows = document.createElement('tr');
            for (let j = 0; j < matrPl2[i].length; j++) {
                tableColumns = document.createElement('td');

                tableColumns.innerHTML += matrPl1[i][j] + ";" + matrPl2[i][j];
                tableRows.appendChild(tableColumns);


            }
            tableG.appendChild(tableRows);

        }
        compTable = tableG;

    }


    function checkRow(currentIndex, nextIndex) {

        var index = [], isDelete;
        for (let j = 0; j < matrPl1.length; j++) {
            if (currentIndex !== matrPl1.length - 1) {
                if (matrPl1[currentIndex][j] < matrPl1[nextIndex][j]) {

                    index[j] = currentIndex;
                }
                else if (matrPl1[currentIndex][j] > matrPl1[nextIndex][j]) {
                    index[j] = nextIndex;
                } else {
                    index[j] = -1;
                }


            }
        }
        isDelete = index.length > 0 ? index[0] : -1;
        for (let i = 0; i < index.length; i++) {
            if (index[i] !== index[i + 1] && i < index.length - 1) {
                isDelete = -1;
            }

        }
        if (isDelete === -1) {
            return nextIndex === (matrPl1.length - 1) ? -1 : checkRow(currentIndex, nextIndex + 1);
        }

        else {
            for (let i = 0; i < matrPl1[isDelete].length; i++) {
                compTable.getElementsByTagName('tr')[isDelete].getElementsByTagName('td')[i].style.background = 'red';
            }
            return isDelete;
        }


    }


    function checkCol(currentIndex, nextIndex) {
        var index = [], isDelete;

        for (let i = 0; i < matrPl2.length; i++) {
            if (currentIndex !== matrPl2.length - 1) {

                if (matrPl2[i][currentIndex] < matrPl2[i][nextIndex]) {
                    index[i] = currentIndex;

                }
                else if (matrPl2[i][currentIndex] > matrPl2[i][nextIndex]) {
                    index[i] = nextIndex;
                }
                else {
                    index[i] = -1;
                }
            }
        }
        isDelete = index.length > 0 ? index[0] : -1;
        for (let i = 0; i < index.length; i++) {
            if (index[i] !== index[i + 1] && i < index.length - 1) {
                isDelete = -1;
            }

        }
        if (isDelete === -1) {
            return nextIndex === (matrPl2[0].length - 1) ? -1 : checkCol(currentIndex, nextIndex + 1);
        }
        else {
            for (let i = 0; i < matrPl2.length; i++) {
                compTable.getElementsByTagName('tr')[i].getElementsByTagName('td')[isDelete].style.background = 'red';
            }
            return isDelete;
        }
    }

    function TheEnd() {
        if (matrPl2.length <= 1 && matrPl1.length <= 1) {
            alert('Гра завершена')
        }
        if (check.col && check.row) {
            alert('Нема ходів');
        }
    }

    function DeletedCols(currentIndex) {
        isPlayer = true;
        var nextIndex = currentIndex + 1,
            index = nextIndex > (matrPl2[0].length - 1) ? -1 : checkCol(currentIndex, nextIndex);

        if (index > -1) {
            for (let i = 0; i < matrPl1.length; i++) {
                matrPl2[i].splice(index, 1);
                matrPl1[i].splice(index, 1);

            }
            updateMatrix();
            check.row = false;
            check.col = false;
        }
        else if (currentIndex < matrPl2.length - 2) {
            DeletedCols(currentIndex + 1);
        }
        else if (!check.row) {
            check.col = true;
            check.row = true;
            DeletedSt(0);
        }

    }


    function DeletedSt(currentIndex) {
        isPlayer = false;
        var nextIndex = currentIndex + 1,
            index = nextIndex > (matrPl1.length - 1) ? -1 : checkRow(currentIndex, nextIndex);
        if (index > -1) {

            matrPl1.splice(index, 1);
            matrPl2.splice(index, 1);
            updateMatrix();
            check.row = false;
            check.col = false;
        }
        else if (currentIndex < matrPl1.length - 1) {
            DeletedSt(currentIndex + 1);
        }
        else if (!check.col) {
            check.col = true;
            check.row = true;
            DeletedCols(0);
        }

    }


    function myDelRow() {
        var tG = document.getElementsByClassName('myTable')[0],
            tr = tG.getElementsByTagName('tr');

        var wrRow = document.getElementsByClassName('wrapper_rows')[0],
            button = wrRow.getElementsByTagName('button');
        for (let i = 0; i < button.length; i++) {
            button[i].addEventListener('click', function (event) {
                if (tr.length > 1) {
                    tG.deleteRow(i);
                    this.remove();
                }


            });

        }
    }

    function myDelCols() {
        var tG = document.getElementsByClassName('myTable')[0];
        var tr = tG.getElementsByTagName('tr');
        var td = tG.getElementsByTagName('td');
        var wrCol = document.getElementsByClassName('wrapper_cols')[0],
            button = wrCol.getElementsByTagName('button');

        for (let i = 0; i < button.length; i++) {
            button[i].addEventListener('click', function (event) {

                for (let index = 0; index < tr.length; index++) {
                    if (td.length> 1) {
                        tr[index].deleteCell(i);
                        this.remove();
                    }
                }



            });

        }

    }

    function getRandomNumber() {
        var from = +document.getElementById('from').value;
        var to = +document.getElementById('to').value;
        var genSt = from + Math.random() * (to + 1 - from);
        genSt = Math.floor(genSt);
        return genSt;
    }

    buttonGeneration.addEventListener('click', function () {
        var playerFirst = player1.value,
            playerSecond = player2.value;

        GenerationTable(playerFirst, playerSecond);
        GenerationTableForMe(playerFirst, playerSecond);
        buttonGeneration.remove();
        myDelRow();
        myDelCols();


    });

    function Challenge() {
        check.row = false;
        check.col = false;
        if (isPlayer) {
            isPlayer = false;
            DeletedSt(0);
        }
        else {
            isPlayer = true;
            DeletedCols(0);
        }

    };

    deleteStr.addEventListener('click', function () {
        Challenge();
        TheEnd();
    });

    function resetDate() {
        matrPl1 = [];
        matrPl2 = [];
        if (tableElement)
            wrapper.removeChild(tableElement);
    };
})();

